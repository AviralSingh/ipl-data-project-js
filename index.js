const fs = require('fs');
const csv = require('csv-parser');
const numberOfMatches = require('./src/server/matches-played-per-year.js')
const matchesWonPerYear = require('./src/server/number-of-matches-won-per-team-per-year.js');
const extraRuns = require('./src/server/extra-runs-per-team-in-2016.js');
const economicalBowlers = require('./src/server/economical-bowlers.js');
const wonToss = require('./src/server/won-toss-and-match.js')
const playerOfTheMatch = require('./src/server/player-of-the-match.js');
const strikeRates = require('./src/server/strike-rate-for-each-season.js');
const dismissals = require('./src/server/highest-dismissals.js');
const economicalSuperOver = require('./src/server/best-economy-in-super-over.js');

const matchesdata = [];
const deliveriesdata = []
fs.createReadStream('src/data/matches.csv')
    .pipe(csv())
    .on('data', (data) => {
        matchesdata.push(data);
    })
    .on('end', () => {
        fs.createReadStream('src/data/deliveries.csv')
            .pipe(csv())
            .on('data', (data) => {
                deliveriesdata.push(data);
            })
            .on('end', () => {

                // problem 1
                let result = numberOfMatches(matchesdata);
                fs.writeFileSync('./src/public/output/bestEconomyInSuperOver.json', JSON.stringify(result, null, 2));

                // problem 2
                result = matchesWonPerYear(matchesdata);
                fs.writeFileSync('./src/public/output/matchesperyear.json', JSON.stringify(result, null, 2));

                // problem 3
                result = extraRuns(deliveriesdata);
                fs.writeFileSync('./src/public/output/extraRuns.json', JSON.stringify(result, null, 2));

                // problem 4
                result = economicalBowlers(deliveriesdata, matchesdata);
                fs.writeFileSync('./src/public/output/top10EconomicBowlers.json', JSON.stringify(result, null, 2));

                // problem 5
                result = wonToss(matchesdata);
                fs.writeFileSync('./src/public/output/wonTossAndMatch.json', JSON.stringify(result, null, 2));

                // problem 6
                result = playerOfTheMatch(matchesdata);
                fs.writeFileSync('./src/public/output/playerOfTheMatch.json', JSON.stringify(result, null, 2));

                // problem 7
                result = strikeRates(deliveriesdata, matchesdata);
                fs.writeFileSync('./src/public/output/strikeRateOverTheSeasons.json', JSON.stringify(result, null, 2));

                // problem 8
                result = dismissals(deliveriesdata);
                fs.writeFileSync('./src/public/output/mostDismissals.json', JSON.stringify(result, null, 2));

                // problem 9
                result = economicalSuperOver(deliveriesdata);
                fs.writeFileSync('./src/public/output/bestEconomyInSuperOver.json', JSON.stringify(result, null, 2));

            });
    });
