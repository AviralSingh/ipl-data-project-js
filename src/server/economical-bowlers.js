function economicalBowlers(deliveriesdata, matchesdata) {

  let startId = 0;
  let endId = 0;
  let flag = false;
  let bowlers = {};

  for (i of matchesdata) {

    if (i.season == 2015 && flag == false) {

      startId = i.match_id;
      flag = true;

    }

    else if (i.season != 2015 && flag == true) {

      endId = i.match_id - 1;
      break;

    }
  }

  for (i of deliveriesdata) {

    if (i.match_id >= startId && i.match_id <= endId && i.is_super_over == 0) {

      if (!bowlers[i.bowler]) {

        bowlers[i.bowler] = { runs: 0, balls: 0 };
        bowlers[i.bowler].runs = Number(i.total_runs) - Number(i.legbye_runs) - Number(i.bye_runs);

        if (i.noball_runs === '0' && i.wide_runs === '0') {
          bowlers[i.bowler].balls = 1;
        }

      }

      else {

        bowlers[i.bowler].runs += Number(i.total_runs) - Number(i.legbye_runs) - Number(i.bye_runs);

        if (i.noball_runs === '0' && i.wide_runs === '0') {
          bowlers[i.bowler].balls += 1;
        }

      }
    }
  }
  for (i in bowlers) {

    bowlers[i] = Number((bowlers[i].runs / (bowlers[i].balls / 6)).toFixed(2));

  }

  bowlers = (Object.entries(bowlers).sort((a, b) => a[1] - b[1])).slice(0, 10);
  bowlers = Object.fromEntries(bowlers);

  return bowlers;
}

module.exports = economicalBowlers;
