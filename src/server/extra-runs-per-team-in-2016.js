function extraRuns(deliveriesdata) {

    let runs = {};

    for (i of deliveriesdata) {

        if (i.match_id > 576) {

            if (!runs[i.bowling_team]) {
                runs[i.bowling_team] = Number(i.extra_runs);
            }

            else {
                runs[i.bowling_team] += Number(i.extra_runs);
            }
        }

    }
    return runs;
}

module.exports = extraRuns;