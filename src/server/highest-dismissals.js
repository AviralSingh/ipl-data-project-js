function dismissals(deliveriesdata) {

    dimissed = {};

    for (i of deliveriesdata) {

        if (i.player_dismissed != 0 && i.dismissal_kind !== 'run out') {

            if (!dimissed[i.batsman]) {
                dimissed[i.batsman] = {};
                dimissed[i.batsman][i.bowler] = 1;
            }

            else {

                if (!dimissed[i.batsman][i.bowler]) {
                    dimissed[i.batsman][i.bowler] = 1;

                }
                else {
                    dimissed[i.batsman][i.bowler] += 1;
                }

            }
        }
    }
    let result = {};
    let max = 0;

    batsmen = Object.keys(dimissed);
    dismissal_counts = Object.values(dimissed);

    for (i in dismissal_counts) {

        sorted_dismissals = (Object.entries(dismissal_counts[i]).sort((a, b) => b[1] - a[1])).slice(0, 1);
        sorted_dismissals = sorted_dismissals[0];

        if (sorted_dismissals[1] > max) {

            max = sorted_dismissals[1];

            for (const key in result) {
                delete result[key];
            }

            let temp = {}
            temp[sorted_dismissals[0]] = max
            result[batsmen[i]] = temp;

        }
    }

    return result;
}

module.exports = dismissals;