function numberOfMatches(matchesdata) {

  matches = {};

  for (data of matchesdata) {

    if (matches[data.season] == undefined) {
      matches[data.season] = 1;
    }

    else {
      matches[data.season] += 1;
    }

  }

  return matches;
}

module.exports = numberOfMatches;
