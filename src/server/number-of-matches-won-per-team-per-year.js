function matchesWonPerYear(matchesdata) {

    const matchesWon = {};

    matchesdata.forEach((data) => {

        if (!matchesWon[data.season]) {
            matchesWon[data.season] = {};
        }

        if (!matchesWon[data.season][data.winner]) {
            matchesWon[data.season][data.winner] = 1;
        }

        else {
            matchesWon[data.season][data.winner] += 1;
        }
        
    })

    return matchesWon;
}

module.exports = matchesWonPerYear;