function playerOfTheMatch(matchesdata) {

    let mostManOfTheMatch = {};
    topPlayer = {}

    for (i of matchesdata) {

        if (!topPlayer[i.season]) {

            topPlayer[i.season] = {};
            topPlayer[i.season][i.player_of_match] = 1;

        }

        else {

            if (!topPlayer[i.season][i.player_of_match]) {
                topPlayer[i.season][i.player_of_match] = 1;
            }
            else {
                topPlayer[i.season][i.player_of_match] += 1;
            }

        }
    }

    let years = Object.keys(topPlayer);
    let val = Object.values(topPlayer);

    for (i in val) {

        val[i] = Object.entries(val[i]);
        val[i] = val[i].sort((a, b) => b[1] - a[1]);

        let names = Object.fromEntries(val[i]);
        firstName = Object.keys(names)[0];

        max = names[firstName];
        mostManOfTheMatch[years[i]] = {};

        for (j in names) {

            if (names[j] == max) {
                mostManOfTheMatch[years[i]][j] = names[j];
            }
            else {
                break;
            }

        }
    }
    return mostManOfTheMatch;
}

module.exports = playerOfTheMatch;