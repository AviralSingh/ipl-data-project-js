function strikeRates(deliveriesdata, matchesdata) {

    let strikeRate = {};
    
    for (i of deliveriesdata) {

        season = function s(matchesdata) {
            
            for (j of matchesdata) {
                if (i.match_id == j.match_id) {
                    return j.season;
                }
            }
        }

        season = season(matchesdata);

        if (!strikeRate[season] && i.batsman === 'V Kohli') {

            if (!(i.noball_runs && i.wide_runs)) {
                strikeRate[season] = { runs: Number(i.batsman_runs), balls: 0 };
            }
            else {
                strikeRate[season] = { runs: Number(i.batsman_runs), balls: 1 };
            }

        }
        else if (i.batsman === 'V Kohli') {

            if (!(i.noball_runs && i.wide_runs)) {
                strikeRate[season].runs+=Number(i.batsman_runs);
            }
            else {
                strikeRate[season].runs+=Number(i.batsman_runs);
                strikeRate[season].balls+=1;
            }
        }
    }

    for(i in strikeRate){
        strikeRate[i].SR=((strikeRate[i].runs/strikeRate[i].balls)*100).toFixed(2);
    }

    console.log('Strike rate of Virat Kohli over the years: ', strikeRate);
    return strikeRate;
}

module.exports = strikeRates;