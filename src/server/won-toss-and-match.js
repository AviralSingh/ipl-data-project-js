function wonToss(matchesdata) {

    winners = {};

    for (i of matchesdata) {

        if (i.toss_winner == i.winner) {

            if (!winners[i.winner]) {
                winners[i.winner] = 1;
            }
            else {
                winners[i.winner] += 1;
            }

        }
    }
    
    return winners;
}

module.exports = wonToss;